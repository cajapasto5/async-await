let XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;
const API ='https://rickandmortyapi.com/api/character/';
function fetchData(url_api,callback) {
    let xhttp  = new XMLHttpRequest();
    xhttp.open('GET', url_api,true);
    xhttp.onreadystatechange = function (event) {
        //si el estado en el que se encuentra es satisfactorio (tiene del 0 al 4)
        if (xhttp.readyState === 4) {
            //estatus en el que se encuentra
            if (xhttp.status === 200) {
                //regreso el callback, que es una respuesta con el texto
                callback(null,JSON.parse(xhttp.responseText));
            }
            else{
                const error = new Error('Error ' + url_api);
                return callback(error,null);
            }
        }
    }
    xhttp.send();
}

//esta función trae la petición a todos, luego a un personaje y de que dimensión viene
//mala práctica como desarrollador, callback hell
fetchData(API, function (error1,data1) {
    if(error1) return console.error(error1)
    fetchData(API + data1.results[0].id,function (error2,data2) {
        if(error2) return console.error(error2);
            fetchData(data2.origin.url,function (error3,data3) {
                if(error3) return console.error(error3);
                console.log(data1.info.count);
                console.log(data2.name);
                console.log(data3.dimension);
            })
        
    })
})