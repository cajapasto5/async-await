let XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;
const fetchData=(url_api)=> {

    return new Promise((resolve,reject)=>{
        const xhttp  = new XMLHttpRequest();
        xhttp.open('GET', url_api,true);
        xhttp.onreadystatechange = ((event)=> {
            //si el estado en el que se encuentra es satisfactorio (tiene del 0 al 4)
            if (xhttp.readyState === 4) {
                //estatus en el que se encuentra (ternaria)
                (xhttp.status === 200)
                    ? resolve(JSON.parse(xhttp.responseText))
                    : reject(new Error('error', url_api))    
            }
        })
        xhttp.send();
    }) 
}

module.exports = fetchData;